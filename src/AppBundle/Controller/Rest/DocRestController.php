<?php

namespace AppBundle\Controller\Rest;

use AppBundle\Entity\Doc;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Util\Codes;



/**
 * @Rest\RouteResource("Doc")
 * @Rest\Prefix("/api")
 */
class DocRestController extends FOSRestController
{

    const ENTITY = 'AppBundle:Doc';

    /**
     * @Rest\Get("api/doc")
     * @return JsonResponse
     * @throws \Exception
     */
    public function getAllDocsAction()
    {

        // get doctrine manager
        $em = $this->getDoctrine()->getManager();
        $doc = $em->getRepository("AppBundle:Doc")->getAllByArray();

        return new JsonResponse($doc);

    }

    /**
     * @Rest\Post("api/doc/find")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function DocViewOneAction(Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            // get request
            $data = $this->transformJsonBody($request);
            $requestData = $data->request->all();

            $transferDoc = $em->getRepository('AppBundle:Doc')->findOneById($requestData["id"]);

            $status = $transferDoc;

        }catch (\Exception $exception){
            $status = array('status' => $exception->getMessage());
        }

        return new JsonResponse($status);
    }

    /**
     * @param Request $request
     * @return Request|null
     */
    protected function transformJsonBody(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            return null;
        }

        if ($data === null) {
            return $request;
        }

        $request->request->replace($data);

        return $request;
    }

    /**
     * @Rest\Post("api/doc")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function DocCreateAction(Request $request)
    {

        try {
            // get request

            $data = $this->transformJsonBody($request);
            $requestData = $data->request->all();
            $em = $this->getDoctrine()->getManager();

            $transferDoc = new Doc();

            $transferDoc->setName($requestData["name"]);
            $transferDoc->setContent($requestData["content"]);
            $transferDoc->setUpdated(new \DateTime());

            $em->persist($transferDoc);
            $em->flush();

            $status = array('status' => true);

        }catch (\Exception $exception){
            $status = array('status' => $exception->getMessage());
        }

        return new JsonResponse($status);
    }

    /**
     * @Rest\Put("api/doc")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function DocEditAction(Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            // get request
            $data = $this->transformJsonBody($request);
            $requestData = $data->request->all();

            $transferDoc = $em->getRepository('AppBundle:Doc')->find($requestData["id"]);

            $transferDoc->setName($requestData["name"]);
            $transferDoc->setContent($requestData["content"]);
            $transferDoc->setUpdated(new \DateTime('now'));

            $em->persist($transferDoc);
            $em->flush();

            $status = array('status' => true);

        }catch (\Exception $exception){
            $status = array('status' => $exception->getMessage());
        }

        return new JsonResponse($status);
    }

    /**
     * @Rest\Delete("api/doc")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function DocDeleteAction(Request $request)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            // get request
            $data = $this->transformJsonBody($request);
            $requestData = $data->request->all();

            $transferDoc = $em->getRepository('AppBundle:Doc')->find($requestData["id"]);

            $em->remove($transferDoc);
            $em->flush();

            $status = array('status' => true);

        }catch (\Exception $exception){
            $status = array('status' => $exception->getMessage());
        }

        return new JsonResponse($status);
    }
}