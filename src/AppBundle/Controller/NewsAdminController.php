<?php

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class NewsAdminController extends Controller
{

    public function watermarkAction()
    {
        $em = $this->getDoctrine()->getManager();

        $id = $this->get('request')->get($this->admin->getIdParameter());

        // the key used to lookup the template
        $templateKey = $id . 'edit';

        $object = $this->admin->getObject($id);

        $gallery = $object->getGallery();

        $watermark = $this->container->get('watermark');

        if (!is_null($gallery))
        {
            foreach($gallery->getGalleryHasMedias() as $item)
            {
                if(is_object($item))
                {
                    $image = $item->getMedia();
                    $wat = $image->getWaterMark();

                    if ($wat != 1)
                    {
                        $name = $image->getProviderReference();

                        $rest = substr($name, -4);
                        $file_type = str_replace('.', '', $rest);

                        $files = $this->readAllFiles($this->searchDir());

                        $the_file = '';
                        foreach($files['files'] as $f)
                        {
                            if (strpos($f,$name) !== false) {
                                $the_file = $f;
                            }
                        }

                        $watermark->AddWatermark($the_file, $file_type, \Application\Sonata\MediaBundle\Tool\Watermark::WATERMARK_BIG);

                        $image->setWatermark(1);
                        $em->persist($image);
                        $em->flush();
                    }
                }
            }

            $this->get('session')->getFlashBag()->set('sonata_flash_success', $this->get('translator')->trans('Watermarks added!'));
        }

        if (!$object)
        {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object))
        {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        if ($this->get('request')->getMethod() == 'POST')
        {
            $form->submit($this->get('request'));

            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved()))
            {
                $this->admin->update($object);
                $this->get('session')->getFlashBag()->set('sonata_flash_success', 'flash_edit_success');

                if ($this->isXmlHttpRequest())
                {
                    return $this->renderJson(array (
                        'result' => 'ok',
                        'objectId' => $this->admin->getNormalizedIdentifier($object)
                    ));
                }

                // redirect to edit mode
                return $this->redirectTo($object);
            }

            // show an error message if the form failed validation
            if (!$isFormValid)
            {
                $this->get('session')->getFlashBag()->set('sonata_flash_error', 'flash_edit_error');
            }
            elseif ($this->isPreviewRequested())
            {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = $id . 'preview';
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->redirect($this->generateUrl('admin_yerevan_main_news_edit', array('id' => $id)));

        return $this->render($this->admin->getTemplate($templateKey), array (
            'action' => 'edit',
            'form' => $view,
            'object' => $object,
        ));
    }

    public function searchDir()
    {
        return __DIR__ . '/../../../../web/uploads/media/';
    }

    /**
     * @param string $root
     * @return array
     */
    function readAllFiles($root = '.'){
        $files  = array('files'=>array(), 'dirs'=>array());
        $directories  = array();
        $last_letter  = $root[strlen($root)-1];
        $root  = ($last_letter == '\\' || $last_letter == '/') ? $root : $root.DIRECTORY_SEPARATOR;

        $directories[]  = $root;

        while (sizeof($directories)) {
            $dir  = array_pop($directories);
            if ($handle = opendir($dir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file == '.' || $file == '..') {
                        continue;
                    }
                    $file  = $dir.$file;
                    if (is_dir($file)) {
                        $directory_path = $file.DIRECTORY_SEPARATOR;
                        array_push($directories, $directory_path);
                        $files['dirs'][]  = $directory_path;
                    } elseif (is_file($file)) {
                        $files['files'][]  = $file;
                    }
                }
                closedir($handle);
            }
        }

        return $files;
    }
}