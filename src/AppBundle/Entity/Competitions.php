<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ExecutionContext;

/**
 * AppBundle\Entity\Competitions
 *
 * @ORM\Table(name="competitions")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\CompetitionsRepository")
 * @Gedmo\Loggable(logEntryClass="AppBundle\Entity\LogEntry")
 * @ORM\HasLifecycleCallbacks()
 */
class Competitions
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string name
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    private $name = '';

    /**
     * @var string slug
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=100, unique=true, nullable=false)
     */
    private $slug;

    /**
     * @var string $title_am
     *
     * @Gedmo\Versioned
     * @ORM\Column(length=255, nullable=true)
     */
    protected $title_am;

    /**
     * @var string $title_en
     *
     * @Gedmo\Versioned
     * @ORM\Column(length=255, nullable=true)
     */
    protected $title_en;

    /**
     * @var string $title_ru
     *
     * @Gedmo\Versioned
     * @ORM\Column(length=255, nullable=true)
     */
    protected $title_ru;

    /**
     * @var string $content_am
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content_am;

    /**
     * @var string $content_en
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content_en;

    /**
     * @var string $content_ru
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content_ru;

    /**
     * @var string $short_content_am
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $short_content_am;

    /**
     * @var string $short_content_en
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $short_content_en;

    /**
     * @var string $short_content_ru
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $short_content_ru;

    /**
     * @var string news_status
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="integer")
     */
    private $news_status;

    /**
     * @var integer $news_order
     * @ORM\Column(name="news_order", type="integer")
     */
    protected $news_order = 1000;

    /**
     * @var \DateTime $start_date
     *
     * @ORM\Column(name="start_date", type="date")
     */
    protected $start_date;

    /**
     * @var \DateTime $news_end_date
     *
     * @ORM\Column(name="news_end_date", type="date")
     */
    protected $news_end_date;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable
     */
    private $updated;

    /**
     * @var integer $image
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     */
    protected $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $path;

    /**
     * @Assert\File(maxSize="60000000")
     */
    protected $file;

    /**
     * @var integer $meta_title
     * @ORM\Column(name="meta_title", type="string", length=200, nullable=true)
     */
    protected $meta_title;

    /**
     * @var integer $meta_desc
     * @ORM\Column(name="meta_desc", type="string", length=300, nullable=true)
     */
    protected $meta_desc;

    /**
     * @var integer $meta_kw
     * @ORM\Column(name="meta_kw", type="string", length=200, nullable=true)
     */
    protected $meta_kw;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Gallery", cascade={"persist"})
     */
    protected $gallery;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->start_date = new \DateTime('today');
        $this->news_end_date = \DateTime::createFromFormat('d-m-Y', date('t-m-Y'));
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return News
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return News
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set titleAm.
     *
     * @param string|null $titleAm
     *
     * @return Announc
     */
    public function setTitleAm($titleAm = null)
    {
        $this->title_am = $titleAm;

        return $this;
    }

    /**
     * Get titleAm.
     *
     * @return string|null
     */
    public function getTitleAm()
    {
        return $this->title_am;
    }

    /**
     * Set titleEn.
     *
     * @param string|null $titleEn
     *
     * @return Announc
     */
    public function setTitleEn($titleEn = null)
    {
        $this->title_en = $titleEn;

        return $this;
    }

    /**
     * Get titleEn.
     *
     * @return string|null
     */
    public function getTitleEn()
    {
        return $this->title_en;
    }

    /**
     * Set titleRu.
     *
     * @param string|null $titleRu
     *
     * @return Announc
     */
    public function setTitleRu($titleRu = null)
    {
        $this->title_ru = $titleRu;

        return $this;
    }

    /**
     * Get titleRu.
     *
     * @return string|null
     */
    public function getTitleRu()
    {
        return $this->title_ru;
    }

    /**
     * Set contentAm.
     *
     * @param string|null $contentAm
     *
     * @return Announc
     */
    public function setContentAm($contentAm = null)
    {
        $this->content_am = $contentAm;

        return $this;
    }

    /**
     * Get contentAm.
     *
     * @return string|null
     */
    public function getContentAm()
    {
        return $this->content_am;
    }

    /**
     * Set contentEn.
     *
     * @param string|null $contentEn
     *
     * @return Announc
     */
    public function setContentEn($contentEn = null)
    {
        $this->content_en = $contentEn;

        return $this;
    }

    /**
     * Get contentEn.
     *
     * @return string|null
     */
    public function getContentEn()
    {
        return $this->content_en;
    }

    /**
     * Set contentRu.
     *
     * @param string|null $contentRu
     *
     * @return Announc
     */
    public function setContentRu($contentRu = null)
    {
        $this->content_ru = $contentRu;

        return $this;
    }

    /**
     * Get contentRu.
     *
     * @return string|null
     */
    public function getContentRu()
    {
        return $this->content_ru;
    }

    /**
     * Set shortContentAm.
     *
     * @param string|null $shortContentAm
     *
     * @return Announc
     */
    public function setShortContentAm($shortContentAm = null)
    {
        $this->short_content_am = $shortContentAm;

        return $this;
    }

    /**
     * Get shortContentAm.
     *
     * @return string|null
     */
    public function getShortContentAm()
    {
        return $this->short_content_am;
    }

    /**
     * Set shortContentEn.
     *
     * @param string|null $shortContentEn
     *
     * @return Announc
     */
    public function setShortContentEn($shortContentEn = null)
    {
        $this->short_content_en = $shortContentEn;

        return $this;
    }

    /**
     * Get shortContentEn.
     *
     * @return string|null
     */
    public function getShortContentEn()
    {
        return $this->short_content_en;
    }

    /**
     * Set shortContentRu.
     *
     * @param string|null $shortContentRu
     *
     * @return Announc
     */
    public function setShortContentRu($shortContentRu = null)
    {
        $this->short_content_ru = $shortContentRu;

        return $this;
    }

    /**
     * Get shortContentRu.
     *
     * @return string|null
     */
    public function getShortContentRu()
    {
        return $this->short_content_ru;
    }

    /**
     * Set newsStatus.
     *
     * @param int $newsStatus
     *
     * @return Announc
     */
    public function setNewsStatus($newsStatus)
    {
        $this->news_status = $newsStatus;

        return $this;
    }

    /**
     * Get newsStatus.
     *
     * @return int
     */
    public function getNewsStatus()
    {
        return $this->news_status;
    }

    /**
     * Set newsOrder.
     *
     * @param int $newsOrder
     *
     * @return Announc
     */
    public function setNewsOrder($newsOrder)
    {
        $this->news_order = $newsOrder;

        return $this;
    }

    /**
     * Get newsOrder.
     *
     * @return int
     */
    public function getNewsOrder()
    {
        return $this->news_order;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return Announc
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set newsEndDate.
     *
     * @param \DateTime $newsEndDate
     *
     * @return Announc
     */
    public function setNewsEndDate($newsEndDate)
    {
        $this->news_end_date = $newsEndDate;

        return $this;
    }

    /**
     * Get newsEndDate.
     *
     * @return \DateTime
     */
    public function getNewsEndDate()
    {
        return $this->news_end_date;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Announc
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Announc
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set path.
     *
     * @param string|null $path
     *
     * @return Announc
     */
    public function setPath($path = null)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path.
     *
     * @return string|null
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set metaTitle.
     *
     * @param string|null $metaTitle
     *
     * @return Announc
     */
    public function setMetaTitle($metaTitle = null)
    {
        $this->meta_title = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle.
     *
     * @return string|null
     */
    public function getMetaTitle()
    {
        return $this->meta_title;
    }

    /**
     * Set metaDesc.
     *
     * @param string|null $metaDesc
     *
     * @return Announc
     */
    public function setMetaDesc($metaDesc = null)
    {
        $this->meta_desc = $metaDesc;

        return $this;
    }

    /**
     * Get metaDesc.
     *
     * @return string|null
     */
    public function getMetaDesc()
    {
        return $this->meta_desc;
    }

    /**
     * Set metaKw.
     *
     * @param string|null $metaKw
     *
     * @return Announc
     */
    public function setMetaKw($metaKw = null)
    {
        $this->meta_kw = $metaKw;

        return $this;
    }

    /**
     * Get metaKw.
     *
     * @return string|null
     */
    public function getMetaKw()
    {
        return $this->meta_kw;
    }

    /**
     * Set image.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $image
     *
     * @return Announc
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set gallery.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Gallery|null $gallery
     *
     * @return Announc
     */
    public function setGallery(\Application\Sonata\MediaBundle\Entity\Gallery $gallery = null)
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Gallery|null
     */
    public function getGallery()
    {
        return $this->gallery;
    }
}
