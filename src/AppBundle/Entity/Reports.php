<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ExecutionContext;

/**
 * AppBundle\Entity\Reports
 *
 * @ORM\Table(name="reports")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ReportsRepository")
 * @Gedmo\Loggable(logEntryClass="AppBundle\Entity\LogEntry")
 * @ORM\HasLifecycleCallbacks()
 */
class Reports
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string name
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    private $name = '';

    /**
     * @var string slug
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=100, unique=true, nullable=false)
     */
    private $slug;

    /**
     * @var string $title_am
     *
     * @Gedmo\Versioned
     * @ORM\Column(length=255, nullable=true)
     */
    protected $title_am;

    /**
     * @var string $title_en
     *
     * @Gedmo\Versioned
     * @ORM\Column(length=255, nullable=true)
     */
    protected $title_en;

    /**
     * @var string $title_ru
     *
     * @Gedmo\Versioned
     * @ORM\Column(length=255, nullable=true)
     */
    protected $title_ru;

    /**
     * @var string $content_am
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content_am;

    /**
     * @var string $content_en
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content_en;

    /**
     * @var string $content_ru
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content_ru;

    /**
     * @var string $short_content_am
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $short_content_am;

    /**
     * @var string $short_content_en
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $short_content_en;

    /**
     * @var string $short_content_ru
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $short_content_ru;

    /**
     * @var string s_status
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="integer")
     */
    private $s_status;

    /**
     * @var integer $s_order
     * @ORM\Column(name="s_order", type="integer")
     */
    protected $s_order = 1000;

    /**
     * @var \DateTime $start_date
     *
     * @ORM\Column(name="start_date", type="date")
     */
    protected $start_date;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable
     */
    private $updated;

    /**
     * @var integer $image
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     */
    protected $image;

    /**
     * @var integer $meta_title
     * @ORM\Column(name="meta_title", type="string", length=200, nullable=true)
     */
    protected $meta_title;

    /**
     * @var integer $meta_desc
     * @ORM\Column(name="meta_desc", type="string", length=300, nullable=true)
     */
    protected $meta_desc;

    /**
     * @var integer $meta_kw
     * @ORM\Column(name="meta_kw", type="string", length=200, nullable=true)
     */
    protected $meta_kw;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->start_date = new \DateTime('today');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return News
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return News
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set titleAm.
     *
     * @param string|null $titleAm
     *
     * @return Reports
     */
    public function setTitleAm($titleAm = null)
    {
        $this->title_am = $titleAm;

        return $this;
    }

    /**
     * Get titleAm.
     *
     * @return string|null
     */
    public function getTitleAm()
    {
        return $this->title_am;
    }

    /**
     * Set titleEn.
     *
     * @param string|null $titleEn
     *
     * @return Reports
     */
    public function setTitleEn($titleEn = null)
    {
        $this->title_en = $titleEn;

        return $this;
    }

    /**
     * Get titleEn.
     *
     * @return string|null
     */
    public function getTitleEn()
    {
        return $this->title_en;
    }

    /**
     * Set titleRu.
     *
     * @param string|null $titleRu
     *
     * @return Reports
     */
    public function setTitleRu($titleRu = null)
    {
        $this->title_ru = $titleRu;

        return $this;
    }

    /**
     * Get titleRu.
     *
     * @return string|null
     */
    public function getTitleRu()
    {
        return $this->title_ru;
    }

    /**
     * Set contentAm.
     *
     * @param string|null $contentAm
     *
     * @return Reports
     */
    public function setContentAm($contentAm = null)
    {
        $this->content_am = $contentAm;

        return $this;
    }

    /**
     * Get contentAm.
     *
     * @return string|null
     */
    public function getContentAm()
    {
        return $this->content_am;
    }

    /**
     * Set contentEn.
     *
     * @param string|null $contentEn
     *
     * @return Reports
     */
    public function setContentEn($contentEn = null)
    {
        $this->content_en = $contentEn;

        return $this;
    }

    /**
     * Get contentEn.
     *
     * @return string|null
     */
    public function getContentEn()
    {
        return $this->content_en;
    }

    /**
     * Set contentRu.
     *
     * @param string|null $contentRu
     *
     * @return Reports
     */
    public function setContentRu($contentRu = null)
    {
        $this->content_ru = $contentRu;

        return $this;
    }

    /**
     * Get contentRu.
     *
     * @return string|null
     */
    public function getContentRu()
    {
        return $this->content_ru;
    }

    /**
     * Set shortContentAm.
     *
     * @param string|null $shortContentAm
     *
     * @return Reports
     */
    public function setShortContentAm($shortContentAm = null)
    {
        $this->short_content_am = $shortContentAm;

        return $this;
    }

    /**
     * Get shortContentAm.
     *
     * @return string|null
     */
    public function getShortContentAm()
    {
        return $this->short_content_am;
    }

    /**
     * Set shortContentEn.
     *
     * @param string|null $shortContentEn
     *
     * @return Reports
     */
    public function setShortContentEn($shortContentEn = null)
    {
        $this->short_content_en = $shortContentEn;

        return $this;
    }

    /**
     * Get shortContentEn.
     *
     * @return string|null
     */
    public function getShortContentEn()
    {
        return $this->short_content_en;
    }

    /**
     * Set shortContentRu.
     *
     * @param string|null $shortContentRu
     *
     * @return Reports
     */
    public function setShortContentRu($shortContentRu = null)
    {
        $this->short_content_ru = $shortContentRu;

        return $this;
    }

    /**
     * Get shortContentRu.
     *
     * @return string|null
     */
    public function getShortContentRu()
    {
        return $this->short_content_ru;
    }

    /**
     * Set sStatus.
     *
     * @param int $sStatus
     *
     * @return Reports
     */
    public function setSStatus($sStatus)
    {
        $this->s_status = $sStatus;

        return $this;
    }

    /**
     * Get sStatus.
     *
     * @return int
     */
    public function getSStatus()
    {
        return $this->s_status;
    }

    /**
     * Set sOrder.
     *
     * @param int $sOrder
     *
     * @return Reports
     */
    public function setSOrder($sOrder)
    {
        $this->s_order = $sOrder;

        return $this;
    }

    /**
     * Get sOrder.
     *
     * @return int
     */
    public function getSOrder()
    {
        return $this->s_order;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return Reports
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Reports
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Reports
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set metaTitle.
     *
     * @param string|null $metaTitle
     *
     * @return Reports
     */
    public function setMetaTitle($metaTitle = null)
    {
        $this->meta_title = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle.
     *
     * @return string|null
     */
    public function getMetaTitle()
    {
        return $this->meta_title;
    }

    /**
     * Set metaDesc.
     *
     * @param string|null $metaDesc
     *
     * @return Reports
     */
    public function setMetaDesc($metaDesc = null)
    {
        $this->meta_desc = $metaDesc;

        return $this;
    }

    /**
     * Get metaDesc.
     *
     * @return string|null
     */
    public function getMetaDesc()
    {
        return $this->meta_desc;
    }

    /**
     * Set metaKw.
     *
     * @param string|null $metaKw
     *
     * @return Reports
     */
    public function setMetaKw($metaKw = null)
    {
        $this->meta_kw = $metaKw;

        return $this;
    }

    /**
     * Get metaKw.
     *
     * @return string|null
     */
    public function getMetaKw()
    {
        return $this->meta_kw;
    }

    /**
     * Set image.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $image
     *
     * @return Reports
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getImage()
    {
        return $this->image;
    }
}
