<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ExecutionContext;

/**
 * AppBundle\Entity\Minister
 *
 * @ORM\Table(name="minister")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\MinisterRepository")
 * @Gedmo\Loggable(logEntryClass="AppBundle\Entity\LogEntry")
 * @ORM\HasLifecycleCallbacks()
 */
class Minister
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string name
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $name = '';

    /**
     * @var string slug
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=100, unique=true, nullable=false)
     */
    private $slug;

    /**
     * @var text $firstname_am
     *
     * @Gedmo\Translatable
     * @Gedmo\Versioned
     * @ORM\Column(length=15, nullable=true)
     */
    protected $firstname_am;

    /**
     * @var text $firstname_ru
     *
     * @Gedmo\Translatable
     * @Gedmo\Versioned
     * @ORM\Column(length=15, nullable=true)
     */
    protected $firstname_ru;

    /**
     * @var text $firstname_en
     *
     * @Gedmo\Translatable
     * @Gedmo\Versioned
     * @ORM\Column(length=15, nullable=true)
     */
    protected $firstname_en;

    /**
     * @var text $lastname_am
     *
     * @Gedmo\Translatable
     * @Gedmo\Versioned
     * @ORM\Column(length=25, nullable=true)
     */
    protected $lastname_am;

    /**
     * @var text $lastname_ru
     *
     * @Gedmo\Translatable
     * @Gedmo\Versioned
     * @ORM\Column(length=25, nullable=true)
     */
    protected $lastname_ru;

    /**
     * @var text $lastname_en
     *
     * @Gedmo\Translatable
     * @Gedmo\Versioned
     * @ORM\Column(length=25, nullable=true)
     */
    protected $lastname_en;

    /**
     * @var text $secondname_am
     *
     * @Gedmo\Translatable
     * @Gedmo\Versioned
     * @ORM\Column(length=100, nullable=true)
     */
    protected $secondname_am;

    /**
     * @var text $secondname_ru
     *
     * @Gedmo\Translatable
     * @Gedmo\Versioned
     * @ORM\Column(length=100, nullable=true)
     */
    protected $secondname_ru;

    /**
     * @var text $secondname_en
     *
     * @Gedmo\Translatable
     * @Gedmo\Versioned
     * @ORM\Column(length=100, nullable=true)
     */
    protected $secondname_en;

    /**
     * @var string $content_am
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content_am;

    /**
     * @var string $content_en
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content_en;

    /**
     * @var string $content_ru
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content_ru;

    /**
     * @var string $short_content_am
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $short_content_am;

    /**
     * @var string $short_content_en
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $short_content_en;

    /**
     * @var string $short_content_ru
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $short_content_ru;

    /**
     * @var datetime $created
     *
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable
     */
    private $updated;

    /**
     * @var integer $image
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     */
    protected $image;

    /**
     * @var integer $meta_title
     * @Gedmo\Translatable
     * @ORM\Column(name="meta_title", type="string", length=200, nullable=true)
     */
    protected $meta_title;

    /**
     * @var integer $meta_desc
     * @Gedmo\Translatable
     * @ORM\Column(name="meta_desc", type="string", length=300, nullable=true)
     */
    protected $meta_desc;

    /**
     * @var integer $meta_kw
     * @Gedmo\Translatable
     * @ORM\Column(name="meta_kw", type="string", length=200, nullable=true)
     */
    protected $meta_kw;

    /**
     * Constructor
     */
    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return News
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return News
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set firstnameAm.
     *
     * @param string|null $firstnameAm
     *
     * @return Minister
     */
    public function setFirstnameAm($firstnameAm = null)
    {
        $this->firstname_am = $firstnameAm;

        return $this;
    }

    /**
     * Get firstnameAm.
     *
     * @return string|null
     */
    public function getFirstnameAm()
    {
        return $this->firstname_am;
    }

    /**
     * Set firstnameRu.
     *
     * @param string|null $firstnameRu
     *
     * @return Minister
     */
    public function setFirstnameRu($firstnameRu = null)
    {
        $this->firstname_ru = $firstnameRu;

        return $this;
    }

    /**
     * Get firstnameRu.
     *
     * @return string|null
     */
    public function getFirstnameRu()
    {
        return $this->firstname_ru;
    }

    /**
     * Set firstnameEn.
     *
     * @param string|null $firstnameEn
     *
     * @return Minister
     */
    public function setFirstnameEn($firstnameEn = null)
    {
        $this->firstname_en = $firstnameEn;

        return $this;
    }

    /**
     * Get firstnameEn.
     *
     * @return string|null
     */
    public function getFirstnameEn()
    {
        return $this->firstname_en;
    }

    /**
     * Set lastnameAm.
     *
     * @param string|null $lastnameAm
     *
     * @return Minister
     */
    public function setLastnameAm($lastnameAm = null)
    {
        $this->lastname_am = $lastnameAm;

        return $this;
    }

    /**
     * Get lastnameAm.
     *
     * @return string|null
     */
    public function getLastnameAm()
    {
        return $this->lastname_am;
    }

    /**
     * Set lastnameRu.
     *
     * @param string|null $lastnameRu
     *
     * @return Minister
     */
    public function setLastnameRu($lastnameRu = null)
    {
        $this->lastname_ru = $lastnameRu;

        return $this;
    }

    /**
     * Get lastnameRu.
     *
     * @return string|null
     */
    public function getLastnameRu()
    {
        return $this->lastname_ru;
    }

    /**
     * Set lastnameEn.
     *
     * @param string|null $lastnameEn
     *
     * @return Minister
     */
    public function setLastnameEn($lastnameEn = null)
    {
        $this->lastname_en = $lastnameEn;

        return $this;
    }

    /**
     * Get lastnameEn.
     *
     * @return string|null
     */
    public function getLastnameEn()
    {
        return $this->lastname_en;
    }

    /**
     * Set secondnameAm.
     *
     * @param string|null $secondnameAm
     *
     * @return Minister
     */
    public function setSecondnameAm($secondnameAm = null)
    {
        $this->secondname_am = $secondnameAm;

        return $this;
    }

    /**
     * Get secondnameAm.
     *
     * @return string|null
     */
    public function getSecondnameAm()
    {
        return $this->secondname_am;
    }

    /**
     * Set secondnameRu.
     *
     * @param string|null $secondnameRu
     *
     * @return Minister
     */
    public function setSecondnameRu($secondnameRu = null)
    {
        $this->secondname_ru = $secondnameRu;

        return $this;
    }

    /**
     * Get secondnameRu.
     *
     * @return string|null
     */
    public function getSecondnameRu()
    {
        return $this->secondname_ru;
    }

    /**
     * Set secondnameEn.
     *
     * @param string|null $secondnameEn
     *
     * @return Minister
     */
    public function setSecondnameEn($secondnameEn = null)
    {
        $this->secondname_en = $secondnameEn;

        return $this;
    }

    /**
     * Get secondnameEn.
     *
     * @return string|null
     */
    public function getSecondnameEn()
    {
        return $this->secondname_en;
    }

    /**
     * Set contentAm.
     *
     * @param string|null $contentAm
     *
     * @return Minister
     */
    public function setContentAm($contentAm = null)
    {
        $this->content_am = $contentAm;

        return $this;
    }

    /**
     * Get contentAm.
     *
     * @return string|null
     */
    public function getContentAm()
    {
        return $this->content_am;
    }

    /**
     * Set contentEn.
     *
     * @param string|null $contentEn
     *
     * @return Minister
     */
    public function setContentEn($contentEn = null)
    {
        $this->content_en = $contentEn;

        return $this;
    }

    /**
     * Get contentEn.
     *
     * @return string|null
     */
    public function getContentEn()
    {
        return $this->content_en;
    }

    /**
     * Set contentRu.
     *
     * @param string|null $contentRu
     *
     * @return Minister
     */
    public function setContentRu($contentRu = null)
    {
        $this->content_ru = $contentRu;

        return $this;
    }

    /**
     * Get contentRu.
     *
     * @return string|null
     */
    public function getContentRu()
    {
        return $this->content_ru;
    }

    /**
     * Set shortContentAm.
     *
     * @param string|null $shortContentAm
     *
     * @return Minister
     */
    public function setShortContentAm($shortContentAm = null)
    {
        $this->short_content_am = $shortContentAm;

        return $this;
    }

    /**
     * Get shortContentAm.
     *
     * @return string|null
     */
    public function getShortContentAm()
    {
        return $this->short_content_am;
    }

    /**
     * Set shortContentEn.
     *
     * @param string|null $shortContentEn
     *
     * @return Minister
     */
    public function setShortContentEn($shortContentEn = null)
    {
        $this->short_content_en = $shortContentEn;

        return $this;
    }

    /**
     * Get shortContentEn.
     *
     * @return string|null
     */
    public function getShortContentEn()
    {
        return $this->short_content_en;
    }

    /**
     * Set shortContentRu.
     *
     * @param string|null $shortContentRu
     *
     * @return Minister
     */
    public function setShortContentRu($shortContentRu = null)
    {
        $this->short_content_ru = $shortContentRu;

        return $this;
    }

    /**
     * Get shortContentRu.
     *
     * @return string|null
     */
    public function getShortContentRu()
    {
        return $this->short_content_ru;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Minister
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Minister
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set metaTitle.
     *
     * @param string|null $metaTitle
     *
     * @return Minister
     */
    public function setMetaTitle($metaTitle = null)
    {
        $this->meta_title = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle.
     *
     * @return string|null
     */
    public function getMetaTitle()
    {
        return $this->meta_title;
    }

    /**
     * Set metaDesc.
     *
     * @param string|null $metaDesc
     *
     * @return Minister
     */
    public function setMetaDesc($metaDesc = null)
    {
        $this->meta_desc = $metaDesc;

        return $this;
    }

    /**
     * Get metaDesc.
     *
     * @return string|null
     */
    public function getMetaDesc()
    {
        return $this->meta_desc;
    }

    /**
     * Set metaKw.
     *
     * @param string|null $metaKw
     *
     * @return Minister
     */
    public function setMetaKw($metaKw = null)
    {
        $this->meta_kw = $metaKw;

        return $this;
    }

    /**
     * Get metaKw.
     *
     * @return string|null
     */
    public function getMetaKw()
    {
        return $this->meta_kw;
    }

    /**
     * Set image.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $image
     *
     * @return Minister
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getImage()
    {
        return $this->image;
    }
}
