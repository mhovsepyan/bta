<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping\Entity;

/**
 * Gedmo\Loggable\Entity\LogEntry
 *
 * @Table(
 *     name="bta_ext_log_entries",
 *  indexes={
 * @index(name="log_class_lookup_idx", columns={"object_class"}),
 * @index(name="log_date_lookup_idx", columns={"logged_at"}),
 * @index(name="log_user_lookup_idx", columns={"username"})
 *  }
 * )
 * @Entity(repositoryClass="Gedmo\Loggable\Entity\Repository\LogEntryRepository")
 *
 * @ORM\HasLifecycleCallbacks
 */
class LogEntry extends \Gedmo\Loggable\Entity\MappedSuperclass\AbstractLogEntry
{

    /**
     * @var text $data
     *
     * @ORM\Column(length=255, nullable=true)
     */
    protected $ip = '123.123.123.123';

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    function prePersistIP()
    {
        $this->ip = $_SERVER['REMOTE_ADDR'];
    }

}