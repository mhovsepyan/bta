<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use AppBundle\Form\Type\FileType;
use AppBundle\Entity\OnlineApp;
use AppBundle\Entity\OnlineAppFiles;
/**
 * @Route("/{_locale}", requirements={"_locale" = "hy|en|ru"})
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="bta_home")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Main:index.html.twig');
    }

}
