<?php


namespace AppBundle\Admin;

use A2lix\TranslationFormBundle\Form\Type\TranslationsFormsType;
use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
//use A2lix\TranslationFormBundle\TranslationForm\TranslationForm;
use AppBundle\Entity\News;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Gedmo\Translator\Translation;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Route\RouteCollection;

class MenuAdmin extends Admin
{

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC', // sort direction
        '_sort_by' => 'created' // field name
    );

    /**
     * Row show configuration
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array('label' => 'ID'))
            ->add('title_am', null, array('label' => 'Title'))
            ->add('content_am', null, array('label' => 'Content'))
            ->add('slug', null, array('label' => 'Slug'))
            ->add('created', null, array('label' => 'Created'))
            ->add('updated', null, array('label' => 'Updated'));
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('General')
                ->with('General')
                    ->add('enable', null, array('label' => 'Status'))
                    ->add('name')
                    ->add('slug', null, array('label' => 'Slug'))
                    ->add('parent_category', null, array('label' => 'Root category', 'query_builder' => function($query) {
                            return $query->createQueryBuilder('c')
                                /*->where('c.parent_category is NULL')*/;
                        },)
                    )
                ->end()
            ->end()
            ->tab('Translations')
                ->with('AM')
                    ->add('title_am', null, array('label' => 'Title'))
                ->end()
                ->with('EN')
                    ->add('title_en', null, array('label' => 'Title'))
                ->end()
                ->with('RU')
                    ->add('title_ru', null, array('label' => 'Title'))
                ->end()
            ->end();

        ;
    }

    /**
     * Fields in list rows search
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('slug');
    }

    /**
     * List show configuration
     *
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title_am', null, array('label' => 'Title'))
            ->addIdentifier('name')
            ->addIdentifier('enable')
            ->add('created', null, array('label' => 'Created'))
            ->add('updated', null, array('label' => 'Updated'))
            ->add('_action', 'actions', array('actions' => array(
                'show' => array(),
                'edit' => array(),
                'delete' => array()
            )));
    }
}