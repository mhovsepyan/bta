<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ExecutionContext;

/**
 * AppBundle\Entity\Video
 *
 * @ORM\Table(name="video")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\VideoRepository")
 * @Gedmo\Loggable(logEntryClass="AppBundle\Entity\LogEntry")
 * @ORM\HasLifecycleCallbacks()
 */
class Video
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string name
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    private $name = '';

    /**
     * @var string slug
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=100, unique=true, nullable=false)
     */
    private $slug;

    /**
     * @var string videourl
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $videourl;

    /**
     * @var string $title_am
     *
     * @Gedmo\Versioned
     * @ORM\Column(length=255, nullable=true)
     */
    protected $title_am;

    /**
     * @var string $title_en
     *
     * @Gedmo\Versioned
     * @ORM\Column(length=255, nullable=true)
     */
    protected $title_en;

    /**
     * @var string $title_ru
     *
     * @Gedmo\Versioned
     * @ORM\Column(length=255, nullable=true)
     */
    protected $title_ru;

    /**
     * @var integer $video_status
     * @Gedmo\Versioned
     * @ORM\Column(name="video_status", type="integer")
     */
    protected $video_status;

    /**
     * @var integer $news_order
     * @ORM\Column(name="video_order", type="integer")
     */
    protected $video_order = 1000;

    /**
     * @var \DateTime $start_date
     *
     * @ORM\Column(name="start_date", type="date")
     */
    protected $start_date;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable
     */
    private $updated;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->start_date = new \DateTime('today');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return News
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return News
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set videourl.
     *
     * @param string|null $videourl
     *
     * @return Video
     */
    public function setVideourl($videourl = null)
    {
        $this->videourl = $videourl;

        return $this;
    }

    /**
     * Get videourl.
     *
     * @return string|null
     */
    public function getVideourl()
    {
        return $this->videourl;
    }

    /**
     * Set titleAm.
     *
     * @param string|null $titleAm
     *
     * @return Video
     */
    public function setTitleAm($titleAm = null)
    {
        $this->title_am = $titleAm;

        return $this;
    }

    /**
     * Get titleAm.
     *
     * @return string|null
     */
    public function getTitleAm()
    {
        return $this->title_am;
    }

    /**
     * Set titleEn.
     *
     * @param string|null $titleEn
     *
     * @return Video
     */
    public function setTitleEn($titleEn = null)
    {
        $this->title_en = $titleEn;

        return $this;
    }

    /**
     * Get titleEn.
     *
     * @return string|null
     */
    public function getTitleEn()
    {
        return $this->title_en;
    }

    /**
     * Set titleRu.
     *
     * @param string|null $titleRu
     *
     * @return Video
     */
    public function setTitleRu($titleRu = null)
    {
        $this->title_ru = $titleRu;

        return $this;
    }

    /**
     * Get titleRu.
     *
     * @return string|null
     */
    public function getTitleRu()
    {
        return $this->title_ru;
    }

    /**
     * Set videoStatus.
     *
     * @param int $videoStatus
     *
     * @return Video
     */
    public function setVideoStatus($videoStatus)
    {
        $this->video_status = $videoStatus;

        return $this;
    }

    /**
     * Get videoStatus.
     *
     * @return int
     */
    public function getVideoStatus()
    {
        return $this->video_status;
    }

    /**
     * Set videoOrder.
     *
     * @param int $videoOrder
     *
     * @return Video
     */
    public function setVideoOrder($videoOrder)
    {
        $this->video_order = $videoOrder;

        return $this;
    }

    /**
     * Get videoOrder.
     *
     * @return int
     */
    public function getVideoOrder()
    {
        return $this->video_order;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return Video
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set created.
     *
     * @param \DateTime $created
     *
     * @return Video
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created.
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated.
     *
     * @param \DateTime $updated
     *
     * @return Video
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated.
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
