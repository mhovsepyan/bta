<?php

namespace AppBundle\Controller\Rest;

use AppBundle\Entity\Doc;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Util\Codes;



/**
 * @Rest\RouteResource("Doc")
 * @Rest\Prefix("/api")
 */
class MenuRestController extends FOSRestController
{

    const ENTITY = 'AppBundle:Eservice';

    /**
     * @Rest\Get("api/menu")
     * @return JsonResponse
     * @throws \Exception
     */
    public function getAllMenuAction()
    {

        // get doctrine manager
        $em = $this->getDoctrine()->getManager();
        $doc = $em->getRepository("AppBundle:Eservice")->findAllEserviceGroupCategories();

        return new JsonResponse($doc);

    }

    /**
     * @param Request $request
     * @return Request|null
     */
    protected function transformJsonBody(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            return null;
        }

        if ($data === null) {
            return $request;
        }

        $request->request->replace($data);

        return $request;
    }

}