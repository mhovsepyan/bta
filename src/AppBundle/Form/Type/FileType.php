<?php
/**
 * Created by PhpStorm.
 * User: aram
 * Date: 8/27/14
 * Time: 3:00 PM
 */

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\File;

class FileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // new assert error for attache file name
        $fileAssert = new File();

        $fileAssert->mimeTypes = array("image/png",
            "image/jpeg",
            "image/jpg",
            "image/gif",
            "application/pdf",
            "application/x-pdf",
            "image/vnd-wap-wbmp",
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document");

        $builder->add('file', 'file', array('constraints' => array(
            $fileAssert )));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
            'error_bubbling' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'yerevan_mainbundle_file';
    }
}