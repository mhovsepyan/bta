<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ExecutionContext;

/**
 * AppBundle\Entity\Btanews
 *
 * @ORM\Table(name="btanews")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\BtanewsRepository")
 * @Gedmo\Loggable(logEntryClass="AppBundle\Entity\LogEntry")
 * @ORM\HasLifecycleCallbacks()
 */
class Btanews
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string name
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    private $name = '';

    /**
     * @var string slug
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=100, unique=true, nullable=false)
     */
    private $slug;

    /**
     * @var string videourl
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $videourl;

    /**
     * @var string $title_am
     *
     * @Gedmo\Versioned
     * @ORM\Column(length=255, nullable=true)
     */
    protected $title_am;

    /**
     * @var string $title_en
     *
     * @Gedmo\Versioned
     * @ORM\Column(length=255, nullable=true)
     */
    protected $title_en;

    /**
     * @var string $title_ru
     *
     * @Gedmo\Versioned
     * @ORM\Column(length=255, nullable=true)
     */
    protected $title_ru;

    /**
     * @var string $content_am
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content_am;

    /**
     * @var string $content_en
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content_en;

    /**
     * @var string $content_ru
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content_ru;

    /**
     * @var string $short_content_am
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $short_content_am;

    /**
     * @var string $short_content_en
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $short_content_en;

    /**
     * @var string $short_content_ru
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    protected $short_content_ru;

    /**
     * @var string news_status
     *
     * @Gedmo\Versioned
     * @ORM\Column(type="integer")
     */
    private $news_status;

    /**
     * @var integer $video_status
     * @Gedmo\Versioned
     * @ORM\Column(name="video_status", type="integer")
     */
    protected $video_status;

    /**
     * @var integer $news_order
     * @ORM\Column(name="news_order", type="integer")
     */
    protected $news_order = 1000;

    /**
     * @var \DateTime $start_date
     *
     * @ORM\Column(name="start_date", type="date")
     */
    protected $start_date;

    /**
     * @var \DateTime $news_end_date
     *
     * @ORM\Column(name="news_end_date", type="date")
     */
    protected $news_end_date;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable
     */
    private $updated;

    /**
     * @var integer $image
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     */
    protected $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $path;

    /**
     * @Assert\File(maxSize="60000000")
     */
    protected $file;

    /**
     * @var integer $meta_title
     * @ORM\Column(name="meta_title", type="string", length=200, nullable=true)
     */
    protected $meta_title;

    /**
     * @var integer $meta_desc
     * @ORM\Column(name="meta_desc", type="string", length=300, nullable=true)
     */
    protected $meta_desc;

    /**
     * @var integer $meta_kw
     * @ORM\Column(name="meta_kw", type="string", length=200, nullable=true)
     */
    protected $meta_kw;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Gallery", cascade={"persist"})
     */
    protected $gallery;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->start_date = new \DateTime('today');
        $this->news_end_date = \DateTime::createFromFormat('d-m-Y', date('t-m-Y'));
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return News
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return News
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set news_status
     *
     * @param integer $newsStatus
     * @return News
     */
    public function setNewsStatus($newsStatus)
    {
        $this->news_status = $newsStatus;

        return $this;
    }

    /**
     * Get news_status
     *
     * @return integer
     */
    public function getNewsStatus()
    {
        return $this->news_status;
    }

    /**
     * Set news_date
     *
     * @param \DateTime $newsDate
     * @return News
     */
    public function setNewsDate($newsDate)
    {
        $this->news_date = $newsDate;

        return $this;
    }

    /**
     * Get news_date
     *
     * @return \DateTime
     */
    public function getNewsDate()
    {
        return $this->news_date;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return News
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return News
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set meta_title
     *
     * @param string $metaTitle
     * @return News
     */
    public function setMetaTitle($metaTitle)
    {
        $this->meta_title = $metaTitle;

        return $this;
    }

    /**
     * Get meta_title
     *
     * @return string
     */
    public function getMetaTitle()
    {
        return $this->meta_title;
    }

    /**
     * Set meta_desc
     *
     * @param string $metaDesc
     * @return News
     */
    public function setMetaDesc($metaDesc)
    {
        $this->meta_desc = $metaDesc;

        return $this;
    }

    /**
     * Get meta_desc
     *
     * @return string
     */
    public function getMetaDesc()
    {
        return $this->meta_desc;
    }

    /**
     * Set meta_kw
     *
     * @param string $metaKw
     * @return News
     */
    public function setMetaKw($metaKw)
    {
        $this->meta_kw = $metaKw;

        return $this;
    }

    /**
     * Get meta_kw
     *
     * @return string
     */
    public function getMetaKw()
    {
        return $this->meta_kw;
    }

    /**
     * Set image.
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media|null $image
     *
     * @return Media
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     *
     * @return type
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @param $locale
     */
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Set news_order
     *
     * @param integer $newsOrder
     * @return News
     */
    public function setNewsOrder($newsOrder)
    {
        $this->news_order = $newsOrder;

        return $this;
    }

    /**
     * Get news_order
     *
     * @return integer
     */
    public function getNewsOrder()
    {
        return $this->news_order;
    }

    /**
     * Set gallery
     *
     * @param \Application\Sonata\MediaBundle\Entity\Gallery $gallery
     * @return News
     */
    public function setGallery(\Application\Sonata\MediaBundle\Entity\Gallery $gallery = null)
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \Application\Sonata\MediaBundle\Entity\Gallery
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * Set news_end_date
     *
     * @param \DateTime $newsEndDate
     * @return News
     */
    public function setNewsEndDate($newsEndDate)
    {
        $this->news_end_date = $newsEndDate;

        return $this;
    }

    /**
     * Get news_end_date
     *
     * @return \DateTime
     */
    public function getNewsEndDate()
    {
        return $this->news_end_date;
    }

    /**
     * Set video_status
     *
     * @param integer $videoStatus
     * @return News
     */
    public function setVideoStatus($videoStatus)
    {
        $this->video_status = $videoStatus;

        return $this;
    }

    /**
     * Get video_status
     *
     * @return integer
     */
    public function getVideoStatus()
    {
        return $this->video_status;
    }

    /**
     *
     * @return type
     */
    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
    }

    /**
     *
     * @return type
     */
    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir() . '/' . $this->path;
    }

    /**
     *
     * @return type
     */
    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    /**
     *
     * @return string
     */
    public function getUploadDir()
    {
        return 'edfiles/images/video';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            // do whatever you want to generate a unique name
            $this->path = sha1(uniqid(mt_rand(), true)) . '.' . $this->file->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move($this->getUploadRootDir(), $this->path);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    /**
     * Set path
     *
     * @param string $path
     * @return News
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return Suggestion
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set start_date
     *
     * @param \DateTime $startDate
     * @return News
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;

        return $this;
    }

    /**
     * Get start_date
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set videourl
     *
     * @param string $videourl
     * @return News
     */
    public function setVideourl($videourl)
    {
        $this->videourl = $videourl;
    
        return $this;
    }

    /**
     * Get videourl
     *
     * @return string 
     */
    public function getVideourl()
    {
        return $this->videourl;
    }

    /**
     * @return string|text
     */
    public function getFeedItemTitle()
    {
        return $this->title;
    }

    /**
     * @return string|text
     */
    public function getFeedItemDescription()
    {
        return $this->short_content;
    }

    /**
     * @return \DateTime|datetime
     */
    public function getFeedItemPubDate()
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getFeedItemLink()
    {
        return '/the_link'.$this->slug;
    }

    /**
     * Set titleAm.
     *
     * @param string|null $titleAm
     *
     * @return News
     */
    public function setTitleAm($titleAm = null)
    {
        $this->title_am = $titleAm;

        return $this;
    }

    /**
     * Get titleAm.
     *
     * @return string|null
     */
    public function getTitleAm()
    {
        return $this->title_am;
    }

    /**
     * Set titleEn.
     *
     * @param string|null $titleEn
     *
     * @return News
     */
    public function setTitleEn($titleEn = null)
    {
        $this->title_en = $titleEn;

        return $this;
    }

    /**
     * Get titleEn.
     *
     * @return string|null
     */
    public function getTitleEn()
    {
        return $this->title_en;
    }

    /**
     * Set titleRu.
     *
     * @param string|null $titleRu
     *
     * @return News
     */
    public function setTitleRu($titleRu = null)
    {
        $this->title_ru = $titleRu;

        return $this;
    }

    /**
     * Get titleRu.
     *
     * @return string|null
     */
    public function getTitleRu()
    {
        return $this->title_ru;
    }

    /**
     * Set contentAm.
     *
     * @param string|null $contentAm
     *
     * @return News
     */
    public function setContentAm($contentAm = null)
    {
        $this->content_am = $contentAm;

        return $this;
    }

    /**
     * Get contentAm.
     *
     * @return string|null
     */
    public function getContentAm()
    {
        return $this->content_am;
    }

    /**
     * Set contentEn.
     *
     * @param string|null $contentEn
     *
     * @return News
     */
    public function setContentEn($contentEn = null)
    {
        $this->content_en = $contentEn;

        return $this;
    }

    /**
     * Get contentEn.
     *
     * @return string|null
     */
    public function getContentEn()
    {
        return $this->content_en;
    }

    /**
     * Set contentRu.
     *
     * @param string|null $contentRu
     *
     * @return News
     */
    public function setContentRu($contentRu = null)
    {
        $this->content_ru = $contentRu;

        return $this;
    }

    /**
     * Get contentRu.
     *
     * @return string|null
     */
    public function getContentRu()
    {
        return $this->content_ru;
    }

    /**
     * Set shortContentAm.
     *
     * @param string|null $shortContentAm
     *
     * @return News
     */
    public function setShortContentAm($shortContentAm = null)
    {
        $this->short_content_am = $shortContentAm;

        return $this;
    }

    /**
     * Get shortContentAm.
     *
     * @return string|null
     */
    public function getShortContentAm()
    {
        return $this->short_content_am;
    }

    /**
     * Set shortContentEn.
     *
     * @param string|null $shortContentEn
     *
     * @return News
     */
    public function setShortContentEn($shortContentEn = null)
    {
        $this->short_content_en = $shortContentEn;

        return $this;
    }

    /**
     * Get shortContentEn.
     *
     * @return string|null
     */
    public function getShortContentEn()
    {
        return $this->short_content_en;
    }

    /**
     * Set shortContentRu.
     *
     * @param string|null $shortContentRu
     *
     * @return News
     */
    public function setShortContentRu($shortContentRu = null)
    {
        $this->short_content_ru = $shortContentRu;

        return $this;
    }

    /**
     * Get shortContentRu.
     *
     * @return string|null
     */
    public function getShortContentRu()
    {
        return $this->short_content_ru;
    }
}
