<?php

namespace AppBundle\Admin;

use A2lix\TranslationFormBundle\Form\Type\TranslationsFormsType;
use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
//use A2lix\TranslationFormBundle\TranslationForm\TranslationForm;
use AppBundle\Entity\News;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Gedmo\Translator\Translation;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Route\RouteCollection;



class MinisterOrdersAdmin extends Admin
{

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC', // sort direction
        '_sort_by' => 'created' // field name
    );

    /**
     * Row show configuration
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
                ->add('id', null, array('label' => 'ID'))
                ->add('title_am', null, array('label' => 'Title'))
                ->add('laws_order', null, array('label' => 'Order'))
                ->add('laws_status', null, array('label' => 'Status'))
                ->add('start_date', 'date', array('label' => 'Start Date', 'format' => 'yyyy-MM-dd', 'widget' => 'single_text'))
                ->add('content_am', null, array('label' => 'Content'))
                ->add('slug', null, array('label' => 'Slug'))
                ->add('created', null, array('label' => 'Created'))
                ->add('updated', null, array('label' => 'Updated'));
    }

    /**
     * List show configuration
     *
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('title_am', null, array('label' => 'Title'))
                ->addIdentifier('name')
                ->addIdentifier('laws_status', null, array('label' => 'Status'))
                ->addIdentifier('laws_order', null, array('label' => 'Order'))
                ->add('start_date', 'date', array('label' => 'Start Date', 'format' => 'yyyy-MM-dd', 'widget' => 'single_text'))
                ->add('created', null, array('label' => 'Created'))
                ->add('updated', null, array('label' => 'Updated'))
                ->add('_action', 'actions', array('actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array()
                )));
    }

    /**
     * Row form edit configuration
     *
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        //$languages = $this->configurationPool->getContainer()->getParameter('languages');

        $formMapper
            ->tab('General')
                ->with('General')
                    ->add('name')
                    ->add('slug', null, array('required' => false))
                    ->add('laws_order', null, array('required' => true, 'label' => 'Order'))
                    ->add('start_date', 'date', array('widget' => 'choice', 'required' => false, 'label' => 'Start date',  'years' => range(2005, 2025)))
                    ->add('laws_status', 'choice', array(
                        'choices' => array('On' => '1', 'Off' => '0'), 'label' => 'Status',
                        'required' => true,
                    ))
                    ->add('image', 'sonata_type_model_list', array('required' => false), array('link_parameters' => array('context' => 'default', 'provider' => 'sonata.media.provider.image')))
                    /*->add('file', 'file', array('required' => false))*/
                ->end()
            ->end()
            ->tab('Translations')
                ->with('AM')
                    ->add('title_am', null, array('label' => 'Title'))
                    ->add('short_content_am', CKEditorType::class, array('label' => 'Short Content'))
                    ->add('content_am', CKEditorType::class, array('label' => 'Content'))
                ->end()
                ->with('EN')
                    ->add('title_en', null, array('label' => 'Title'))
                    ->add('short_content_en', CKEditorType::class, array('label' => 'Short Content'))
                    ->add('content_en', CKEditorType::class, array('label' => 'Content'))
                ->end()
                ->with('RU')
                    ->add('title_ru', null, array('label' => 'Title'))
                    ->add('short_content_ru', CKEditorType::class, array('label' => 'Short Content'))
                    ->add('content_ru', CKEditorType::class, array('label' => 'Content'))
                ->end()

            ->end();
    }

    /**
     * Fields in list rows search
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name')
                ->add('start_date', null, array('label' => 'Start Date'))
                ->add('slug');
    }

    /**
     * @param $object
     * @throws \Exception
     */
    public function preUpdate($object)
    {
        $object->setUpdated(new \DateTime("now"));
    }

}