<?php

namespace AppBundle\Admin;

use A2lix\TranslationFormBundle\Form\Type\TranslationsFormsType;
use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
//use A2lix\TranslationFormBundle\TranslationForm\TranslationForm;
use AppBundle\Entity\News;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Gedmo\Translator\Translation;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Route\RouteCollection;



class VideoAdmin extends Admin
{
    public $supportsPreviewMode = true;

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('watermark', 'watermark');
        $collection->add('video_order', 'video_order');
    }

    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC', // sort direction
        '_sort_by' => 'created' // field name
    );

    public function getTemplate($name)
    {
        switch ($name) {
            case 'preview':
                return 'AppBundle:Admin:newsPreview.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    /**
     * Row show configuration
     *
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     * @return void
     */
    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
                ->add('id', null, array('label' => 'ID'))
                ->add('title_am', null, array('label' => 'Title'))
                ->add('video_order', null, array('label' => 'Video order'))
                ->add('video_status', null, array('label' => 'Video Status'))
                ->add('start_date', 'date', array('label' => 'News Start Date', 'format' => 'yyyy-MM-dd', 'widget' => 'single_text'))
                ->add('slug', null, array('label' => 'Slug'))
                ->add('created', null, array('label' => 'Created'))
                ->add('updated', null, array('label' => 'Updated'));
    }

    /**
     * List show configuration
     *
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('title_am', null, array('label' => 'Title'))
                ->addIdentifier('name')
                ->addIdentifier('video_status', null, array('label' => 'Video Status'))
                ->addIdentifier('video_order', null, array('label' => 'Video Order'))
                ->add('start_date', 'date', array('label' => 'News Start Date', 'format' => 'yyyy-MM-dd', 'widget' => 'single_text'))
                ->add('created', null, array('label' => 'Created'))
                ->add('updated', null, array('label' => 'Updated'))
                ->add('_action', 'actions', array('actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array()
                )));
    }

    /**
     * Row form edit configuration
     *
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        //$languages = $this->configurationPool->getContainer()->getParameter('languages');

        $formMapper
            ->tab('General')
                ->with('General')
                    ->add('name')
                    ->add('slug', null, array('required' => false))
                    ->add('video_order', null, array('required' => true))
                    ->add('start_date', 'date', array('widget' => 'choice', 'required' => false, 'years' => range(2005, 2025)))
                    ->add('videourl')
                    ->add('video_status', 'choice', array(
                        'choices' => array('Yes' => '1', 'No' => '1'),
                        'required' => true,
                    ))
                ->end()
            ->end()
            ->tab('Translations')
                ->with('AM')
                    ->add('title_am', null, array('label' => 'Title'))
                ->end()
                ->with('EN')
                    ->add('title_en', null, array('label' => 'Title'))
                ->end()
                ->with('RU')
                    ->add('title_ru', null, array('label' => 'Title'))
                ->end()

            ->end();
    }

    /**
     * Fields in list rows search
     *
     * @param \Sonata\AdminBundle\Datagrid\DatagridMapper $datagridMapper
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name')
                ->add('start_date', null, array('label' => 'Start Date'))
                ->add('slug');
    }

    /**
     * @param $object
     * @throws \Exception
     */
    public function preUpdate($object)
    {
        $object->setUpdated(new \DateTime("now"));
    }

}